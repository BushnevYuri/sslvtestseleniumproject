package com.bushnev.yuri.selenium.models;

public class Advert {
    public String price;
    public String name;

    public Advert(String name, String price) {
        this.price = price;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Advert{" +
                "price='" + price + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
