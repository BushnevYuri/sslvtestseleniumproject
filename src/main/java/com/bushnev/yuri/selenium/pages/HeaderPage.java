package com.bushnev.yuri.selenium.pages;

import com.bushnev.yuri.selenium.framework.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HeaderPage extends BasePage {
    @FindBy(xpath = SEARCH_MENU_LINK_XPATH)
    public WebElement searchMenuLink;

    public SearchPage goToSearchPage() {
        searchMenuLink.click();
        return initPage(SearchPage.class);
    }

    public static final String SEARCH_MENU_LINK_XPATH = "(//span[@class='page_header_menu']/b)[3]";
}
