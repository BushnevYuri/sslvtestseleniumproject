package com.bushnev.yuri.selenium.pages;

import com.bushnev.yuri.selenium.framework.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
    @FindBy(xpath = SWITCH_LANGUAGE_LINK_XPATH)
    public WebElement switchLanguageLink;

    private final int languagesNumbers = 2;

    public void switchLanguage(SiteLanguage language) {
        for (int i = 1; i <= languagesNumbers; i++){
            if (switchLanguageLink.getText().equals(language.toString())){
                switchLanguageLink.click();
                return;
            }
            else
                switchLanguageLink.click();
        }
    }

    public CategoryPage openCategory(String category){
        String locator = String.format("//div[@class='main_head2']/h2/a[contains(text(),'%s')]", category);
        driver.findElement(By.xpath(locator)).click();
        return initPage(CategoryPage.class);
    }

    public enum SiteLanguage {
        LV, RU
    }

    public static final String SWITCH_LANGUAGE_LINK_XPATH = "//span[contains(@class,'menu_lang')]/a";
}
