package com.bushnev.yuri.selenium.pages;

import com.bushnev.yuri.selenium.framework.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class SearchPage extends BasePage {
    @FindBy(id = SEARCH_BUTTON_ID)
    public WebElement searchButton;

    public WebElement getInputElement(String label){
        return driver.findElement(By.xpath(
                String.format("//tr/td[contains(text(),'%s')]/../td[2]/input", label)));
    }

    public List<WebElement> getInputElements(String label){
        return driver.findElements(By.xpath(
                String.format("//tr/td[contains(text(),'%s')]/../td[2]/input", label)));
    }

    public Select getSelectElement(String label){
        return new Select(driver.findElement(By.xpath(
                String.format("//tr/td[contains(text(),'%s')]/../td[2]/select", label))));
    }

    public static final String SEARCH_BUTTON_ID = "sbtn";
}
