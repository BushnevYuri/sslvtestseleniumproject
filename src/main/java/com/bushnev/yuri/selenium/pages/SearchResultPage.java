package com.bushnev.yuri.selenium.pages;

import com.bushnev.yuri.selenium.framework.BasePage;
import com.bushnev.yuri.selenium.models.Advert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class SearchResultPage extends BasePage {
    @FindBy(id = SEARCH_INPUT_ID)
    public WebElement searchInput;

    @FindBy(xpath = DETAILED_SEARCH_LINK_XPATH)
    public WebElement detailedSearchLink;

    @FindBy(xpath = SORT_BY_DROPDOWN_XPATH)
    private WebElement sortByDropdown;

    @FindBy(xpath = DEAL_TYPE_DROPDOWN_XPATH)
    private WebElement dealTypeDropdown;

    @FindBy(xpath = RESULT_ROWS_XPATH)
    private WebElement[] resultRowElements;

    @FindBy(xpath = SEARCH_RESULT_ROWS_XPATH)
    public WebElement[] searchResultRowElements;

    @FindBy(id = SHOW_SELECTED_ELEMENTS_ID)
    public WebElement showSelectedElementsLink;

    public Select sortByDropdownElement(){
        return new Select(sortByDropdown);
    }
    public Select dealTypeDropdownElement(){
        return new Select(dealTypeDropdown);
    }

    public int getResultRowsCount(){
        return driver.findElements(By.xpath(SEARCH_RESULT_ROWS_XPATH)).size();
    }

    public String getAdvertDescriptionByRow(int rowNumber){
        return driver.findElement(By.xpath(
                String.format(ADVERT_ROW_DESCRIPTION_XPATH, rowNumber))).getText();
    }

    public String getAdvertPriceByRow(int rowNumber){
        return driver.findElement(By.xpath(
                String.format(ADVERT_ROW_PRICE_XPATH, rowNumber))).getText();
    }

    public boolean isAdvertShown(Advert advert){
        //Workaround for founded bug. Html is not properly shown for some of adverts
        String q = String.format(SHOWN_ELEMENT_XPATH, advert.name.substring(0,5), advert.price.substring(0,5));
        return driver.findElement(By.xpath(q)).isDisplayed();
    }

    public Advert selectRandomAdvertFromPage(){
        List<WebElement> resultRowElements = driver.findElements(By.xpath(RESULT_ROWS_XPATH));
        int elementsCount = resultRowElements.size();
        int randomNum = (int)(Math.random() * elementsCount);
        if (randomNum == 0)
            randomNum++;
        while(driver.findElement(By.xpath(
                String.format("(%s/td[1]/input)[%d]", RESULT_ROWS_XPATH, randomNum))).isSelected()){
            randomNum = (int)(Math.random() * elementsCount);
            if (randomNum == 0)
                randomNum++;
        }
        driver.findElement(By.xpath(
                String.format("(%s/td[1]/input)[%d]", RESULT_ROWS_XPATH, randomNum))).click();
        Advert advert = new Advert(getAdvertDescriptionByRow(randomNum),
                getAdvertPriceByRow(randomNum));

        return advert;
    }

    public static final String SEARCH_INPUT_ID = "ptxt";
    public static final String DETAILED_SEARCH_LINK_XPATH = "//td/a[@class='a9a' and contains(@href,'search')]";
    public static final String SORT_BY_DROPDOWN_XPATH = "//div[contains(@class,'filter_second_line_dv')]/span[3]/select";
    public static final String DEAL_TYPE_DROPDOWN_XPATH = "//div[contains(@class,'filter_second_line_dv')]/span[4]/select";
    public static final String RESULT_ROWS_XPATH = "//td/table/tbody[form]/tr";
    public static final String SEARCH_RESULT_ROWS_XPATH = "//form[@id='filter_frm']/table/tbody/tr[not(contains(@id,'head_line'))]";
    public static final String SHOW_SELECTED_ELEMENTS_ID = "show_selected_a";
    public static final String SHOWN_ELEMENT_XPATH = "//table/tbody/tr[td[3]/div//*[contains(text(),'%s')] and td[7]//*[contains(text(),'%s')]]";
    public static final String ADVERT_ROW_DESCRIPTION_XPATH = "(//td/table/tbody[form]/tr/td/div[contains(@class,'d1')]/a)[%d]";
    public static final String ADVERT_ROW_PRICE_XPATH = "(//td/table/tbody[form]/tr/td[8])[%d]";




}
