package com.bushnev.yuri.selenium.pages;

import com.bushnev.yuri.selenium.framework.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CategoryPage extends BasePage {
    @FindBy(xpath = CURRENT_CATEGORY_LINK_XPATH)
    public WebElement CurrentCategoryLink;

    public SearchResultPage openSubCategory(String subcategory){
        driver.findElement(By.xpath(
                String.format("//a[contains(@class,'a_category') and contains(text(),'%s')]", subcategory))).click();
        return initPage(SearchResultPage.class);
    }

    public static final String CURRENT_CATEGORY_LINK_XPATH = "//h2[@class='headtitle']/a";
}
