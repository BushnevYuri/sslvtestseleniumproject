package com.bushnev.yuri.selenium.framework;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class BaseTest {
    private static Settings settings = new Settings();

    @BeforeSuite(alwaysRun = true)
    public static void beforeSuite() {
        BasePage.driver = settings.getDriver();
        BasePage.settings = settings;

        BasePage.driver.get(settings.getBaseUrl());
        BasePage.driver.manage().window().maximize();
    }

    @AfterSuite(alwaysRun = true)
    public static void afterClass() {
        BasePage.driver.close();
    }

    @AfterMethod public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            System.out.println(testResult.getStatus());
            File scrFile = ((TakesScreenshot)BasePage.driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(settings.getFailedTestsScreenshotsPath() + testResult.getName() +
                    " " + new Date().getTime()  + ".png"));
        }
    }
}
