package com.bushnev.yuri.selenium.framework;

public class UnknownPropertyException extends RuntimeException {
    public UnknownPropertyException(String message) {
        super(message);
    }
}
