package com.bushnev.yuri.selenium.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.io.*;
import java.util.Properties;

public class Settings {
    private static final String SELENIUM_BASEURL = "selenium.baseUrl";
    private static final String SELENIUM_BROWSER = "selenium.browser";
    private static final String SELENIUM_CHROME_DIVER = "selenium.chromeDriver";
    private static final String SELENIUM_PROPERTIES = "selenium.properties";
    private static final String FAILED_TESTS_SCREENSHOTS_PATH = "selenium.failed.screenshots";

    private String baseUrl;
    private String chromeDriver;
    private BrowserType browser;
    private String failedTestsScreenshotsPath;
    private Properties properties = new Properties();

    public Settings() {
        loadSettings();
    }

    private void loadSettings() {
        properties = loadPropertiesFile();
        baseUrl = getPropertyOrThrowException(SELENIUM_BASEURL);
        browser = BrowserType.Browser(getPropertyOrThrowException(SELENIUM_BROWSER));
        chromeDriver = getPropertyOrThrowException(SELENIUM_CHROME_DIVER);
        failedTestsScreenshotsPath = getPropertyOrThrowException(FAILED_TESTS_SCREENSHOTS_PATH);
    }

    private Properties loadPropertiesFile() {
        try {
            String filename = getPropertyOrThrowException(SELENIUM_PROPERTIES);
            if (filename == null) {
                filename = SELENIUM_PROPERTIES;
            }
            InputStream stream = getClass().getClassLoader().getResourceAsStream(filename);
            if (stream == null) {
                stream = new FileInputStream(new File(filename));
            }
            Properties result = new Properties();
            result.load(stream);
            return result;
        } catch (IOException e) {
            throw new UnknownPropertyException("Property file is not found");
        }
    }


    public String getPropertyOrThrowException(String name) {
        return getProperty(name, true);
    }

    private String getProperty(String name, boolean forceExceptionIfNotDefined) {
        String result;
        if ((result = System.getProperty(name, null)) != null && result.length() > 0) {
            return result;
        } else if ((result = getPropertyFromPropertiesFile(name)) != null && result.length() > 0) {
            return result;
        } else if (forceExceptionIfNotDefined) {
            throw new UnknownPropertyException("Unknown property: [" + name + "]");
        }
        return result;
    }

    private String getPropertyFromPropertiesFile(String name) {
        Object result = properties.get(name);
        if (result == null) {
            return null;
        } else {
            return result.toString();
        }
    }

    public WebDriver getDriver() {
        return getDriver(browser);
    }

    private WebDriver getDriver(BrowserType browserType) {
        switch (browserType) {
            case FIREFOX:
                return new FirefoxDriver();
            case GC:
                System.setProperty("webdriver.chrome.driver", getChromeDriver());
                return new ChromeDriver();
            default:
                throw new UnknownBrowserException("Cannot create driver for unknown browser type");
        }
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getChromeDriver() {
        return chromeDriver;
    }

    public String getFailedTestsScreenshotsPath() {
        return failedTestsScreenshotsPath;
    }
}
