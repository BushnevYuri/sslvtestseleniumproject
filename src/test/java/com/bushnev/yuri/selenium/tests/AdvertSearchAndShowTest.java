package com.bushnev.yuri.selenium.tests;

import com.bushnev.yuri.selenium.framework.BaseTest;
import com.bushnev.yuri.selenium.models.Advert;
import com.bushnev.yuri.selenium.pages.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.bushnev.yuri.selenium.framework.BasePage.initPage;
import static org.junit.Assert.*;

/*  MAIN POINTS
1)Переключение языка сделано безопасно и работает независимо от начального значения языка
2)Меню сделано как отдельный независимый page
3)Так как Input и Select элементов на странице поиска объявлений много, то вместо локаторов сделан метод,
который возвращает необходимые локаторы по label'у
4)В "\src\test\resources\selenium.properties" находятся базовые настройки
5)В "\src\test\resources\failed_tests_screenshots" складываются скриншоты, если тест завалился. Имя = имя теста + timestamp
6)Локаторы сделаны не специфичными для языка. Использяя language bundle можно сделать ещё лучше, но
подумал, что в контексте тестовго задания так запариваться не стоит =)
7)На сайте найдено пару багов (различное поведение по разными браузерами/некорректное отображение некоторые html элементов)
 */

public class AdvertSearchAndShowTest extends BaseTest {
    private HomePage homePage;
    private HeaderPage headerPage;
    private SearchPage searchPage;
    private SearchResultPage searchResultPage;

    @BeforeMethod(alwaysRun = true)
    public void Setup() {
        homePage = initPage(HomePage.class);
        headerPage = initPage(HeaderPage.class);
    }

    @Test
    public void verifyDetailedSearch() {
        homePage.switchLanguage(HomePage.SiteLanguage.RU);
        assertEquals("Couldn't switch language", "LV", homePage.switchLanguageLink.getText());

        CategoryPage categoryPage = homePage.openCategory("Транспорт");
        assertEquals("Enable to open category", "Транспорт" , categoryPage.CurrentCategoryLink.getText());

        searchResultPage = categoryPage.openSubCategory("Обмен легковых авто");
        searchPage = headerPage.goToSearchPage();
        searchPage.getSelectElement("Тип двигателя").selectByVisibleText("Бензин");
        searchPage.getSelectElement("Руль").selectByVisibleText("Регулируемый");
        searchPage.getInputElement("Искомое слово или фраза").sendKeys("Skoda");
        searchPage.searchButton.click();

        searchResultPage.sortByDropdownElement().selectByVisibleText("Цена");
        searchResultPage.dealTypeDropdownElement().selectByVisibleText("Продажа");

        searchResultPage.detailedSearchLink.click();
        searchPage.getInputElements("Цена").get(0).sendKeys("0");
        searchPage.getInputElements("Цена").get(1).sendKeys("5000");
        searchPage.searchButton.click();

        List<Advert> testedAdverts = new ArrayList<Advert>();
        while (testedAdverts.size() < 3)
            testedAdverts.add(searchResultPage.selectRandomAdvertFromPage());

        searchResultPage.showSelectedElementsLink.click();
        assertEquals("Result search should contains 3 adverts" ,3, searchResultPage.getResultRowsCount());
        for (Advert advert : testedAdverts){
            assertTrue("Advert is not presented on page: " + advert ,searchResultPage.isAdvertShown(advert));
        }
    }
}
